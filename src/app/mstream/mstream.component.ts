import { Component, OnInit, HostListener } from '@angular/core';


@Component({
  selector: 'app-mstream',
  templateUrl: './mstream.component.html',
  styleUrls: ['./mstream.component.scss']
})
export class MstreamComponent implements OnInit {

  public readConversation:boolean=false;
  plusCount: number = 0;
  displayTopics: boolean;
  optionsCount: number = 0;
  openedChatName: any;
  dummy:boolean=false;
  replyClicked:boolean=false;
  chatOpen:boolean=true;
  constructor() { }

  ngOnInit(): void {
    this.showChat = 1
    this.openedChatName = '@Abhijit  NS'
  }

  showSideNav = -1
  displayAttachmentOptions(num){
    if (this.showSideNav === num) {
      this.showSideNav = -1;
    } else {
      this.showSideNav = num;
    }
      }

      @HostListener('document:click', ['$event']) clickout(event) {
        this.showSideNav = -1
      }

      showTopicsSidebar(){
        this.displayTopics = true;

      }

      hideTopicsSidebar(){
        this.displayTopics = false;

      }

      showChat = -1;
  selectChat(i,chatName) {
    this.showChat = i
      this.openedChatName = chatName
   
  }

  replyEmail() {
    this.replyClicked=true;
    let emailContent = "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Soluta, sapiente. Dolorem laborum non, ipsa laudantium quia, iure quis consectetur id voluptatibus, dolorum eius? Ut ab amet deleniti, ipsa in voluptate.Lorem ipsum dolor, sit amet consectetur adipisicing elit. Soluta, sapiente. Dolorem laborum non, ipsa laudantium quia, iure quis consectetur id voluptatibus, dolorum eius? Ut ab amet deleniti, ipsa in voluptate.Lorem ipsum dolor, sit amet consectetur adipisicing elit. Soluta, sapiente. Dolorem laborum non, ipsa laudantium quia, iure quis consectetur id voluptatibus, dolorum eius? Ut ab amet deleniti, ipsa in voluptate.Lorem ipsum dolor, sit amet consectetur adipisicing elit. Soluta, sapiente. Dolorem laborum non, ipsa laudantium quia, iure quis consectetur id voluptatibus, dolorum eius? Ut ab amet deleniti, ipsa in voluptate."
    setTimeout(function () {
      document.getElementById('emailContent').innerHTML=emailContent;
      document.getElementById('emailContent').focus();
    },100);
    
    document.onclick = (event) => {
      let target=<HTMLInputElement>event.target;
      let classList = target.parentElement.classList;
      let classEmailReplySection=classList.contains('emailReplySection');
      let classIonsCol=classList.contains('iconsCol');
      if (!classEmailReplySection && !classIonsCol && (target.className!='form-control')) {
        this.replyClicked=false;
      }
    }
  }

  openChat(){
    this.chatOpen=!this.chatOpen;
  }
}
