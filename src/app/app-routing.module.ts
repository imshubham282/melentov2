import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MstreamComponent} from './mstream/mstream.component'


const routes: Routes = [
  {
    path:'mstream',
    component:MstreamComponent


}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
